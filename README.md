# Info

## Rathaus RW
```
Montag geschlossen
Di.  7:30 - 12:00 Uhr
Mi.  9:00 - 12:00 Uhr
Do. 15:30 - 19:00 Uhr
Fr.  9:00 - 12:00 Uhr
```

## Finanzamt NT
```
Mo.  7:30 - 15:30 Uhr
Di.  7:30 - 13:00 Uhr
Mi.  7:30 - 13:00 Uhr
Do.  7:30 - 17:30 Uhr
Fr.  7:30 - 12:00 Uhr
```

